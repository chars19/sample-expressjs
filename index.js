var express = require('express'),
    app = express(),
    port = 3000;

app.get('/', function(req, res){
    res.send('Hello world');
})

var server = app.listen(port, function(){
    var host = server.address().address;
    var port = server.address().port;

    console.log('Example app listening at http://%s:%s', host, port);
})
